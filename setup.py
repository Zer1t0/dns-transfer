import setuptools

name = "dns-transfer"
libname = name.replace("-", "_")

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

version_file = "{}/version.py".format(libname)
with open(version_file) as fi:
    vs = {}
    exec(fi.read(), vs)
    __version__ = vs["__version__"]


setuptools.setup(
    name=name,
    version=__version__,
    author="Eloy Pérez",
    author_email="zer1t0ps@protonmail.com",
    description="Check if DNS zone transfer is allowed",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    url="https://gitlab.com/zer1t0/{}".format(name),
    entry_points={
        "console_scripts": [
            "{} = {}.main:main".format(name, libname),
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)
